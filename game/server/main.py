import uuid

from twisted.internet import reactor
from twisted.internet.protocol import ServerFactory, Protocol


def main(args=None, config=None):
    print('Starting server...')
    port = config['port']
    reactor.listenTCP(port, ClientHandlerFactory())
    print('Server listenning on port: {}'.format(port))
    reactor.run()
    print('Server stopped')


class ClientHandler(Protocol):

    def __init__(self, clients):
        self._id = uuid.uuid4()
        self._clients = clients

    def connectionMade(self):
        print("Connection made")
        self._clients[self._id] = self

    def dataReceived(self, data):
        for cli_id, cli in self._clients.items():
            if not cli_id == self._id:
                cli.transport.write(data)

    def connectionLost(self, failure):
        print('Connection lost')
        if self._id in self._clients:
            del self._clients[self._id]


class ClientHandlerFactory(ServerFactory):

    def __init__(self):
        self._clients = {}

    def buildProtocol(self, addr):
        return ClientHandler(self._clients)


if __name__ == '__main__':
    main()
