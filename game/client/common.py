import pygame
from noise import pnoise2
from pygame import Rect, Surface
from pygame.math import Vector2


class Color(object):
    WHITE = (255, 255, 255, 255)
    BLACK = (0, 0, 0, 255)
    LIGHT_GREY = (191, 191, 191, 255)
    GREY = (127, 127, 127, 255)
    DARK_GREY = (63, 63, 63, 255)
    TRANSPARENT = (0, 0, 0, 0)
    BRIGHT_RED = (255, 0, 0, 255)
    DARK_RED = (63, 0, 0, 255)
    BRIGHT_GREEN = (0, 255, 0, 255)
    DARK_GREEN = (0, 63, 0, 255)
    BRIGHT_BLUE = (0, 0, 255, 255)
    DARK_BLUE = (0, 0, 63, 255)


class Viewport(object):

    def __init__(self, surface: Surface = None, boundaries: Rect = None):
        self._surface = surface if surface else pygame.display.get_surface()
        self._boundaries = boundaries
        self._rect = self._surface.get_rect()

    def get_surface(self):
        return self._surface

    def get_boundaries(self):
        return self._boundaries

    def set_boundaries(self, boundaries_rect):
        self._boundaries = boundaries_rect

    def clear(self):
        self._surface.fill(Color.BLACK)

    def focus(self, position, lerp=1):
        destination = Vector2(position)
        cur_center = Vector2(self._rect.center)
        new_center = cur_center.lerp(destination, lerp)
        self._rect.center = (int(new_center.x), int(new_center.y))

        if self._boundaries:
            self._apply_boundaries()

        return new_center

    def render_sprite(self, sprite, position):
        sprite.draw(self._surface, self._translate_position(position))

    def render_text(self, text, position, static=True, color=Color.WHITE):
        surface = pygame.font.SysFont(None, 32).render(text, True, color)

        if static:
            self._surface.blit(surface, position)
        else:
            self._surface.blit(surface, self._translate_position(position))

    def update(self):
        pygame.display.flip()

    def _translate_position(self, position):
        return Vector2(position) - Vector2(self._rect.topleft)

    def _apply_boundaries(self):
        if self._rect.left < self._boundaries.left:
            self._rect.left = self._boundaries.left
        if self._rect.right > self._boundaries.right:
            self._rect.right = self._boundaries.right
        if self._rect.top < self._boundaries.top:
            self._rect.top = self._boundaries.top
        if self._rect.bottom > self._boundaries.bottom:
            self._rect.bottom = self._boundaries.bottom


class TerrainGenerator(object):

    def __init__(self):
        self._scale = 20
        self._octaves = 3

    def elevation_at(self, position):
        (x, y) = position
        x = x / self._scale
        y = y / self._scale
        return pnoise2(x, y, octaves=self._octaves) + 0.5
