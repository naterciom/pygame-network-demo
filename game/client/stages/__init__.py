"""
Base for stages
"""
from game.client.actors import Actor


class StageOrchestrator(object):

    def __init__(self, viewport):
        self._viewport = viewport
        self._stages = {}
        self._current_stage = None

    def add_stage(self, stage):
        self._stages[stage.get_name()] = stage

        if not self._current_stage:
            self._current_stage = stage

    def next_step(self, delta_time):
        if not self._current_stage.is_started():
            self._current_stage.start()

        next_stage = self._current_stage.step(self._viewport, delta_time)

        if self._current_stage.get_name() != next_stage:
            self._current_stage.stop()
            if next_stage:
                self._current_stage = self._stages[next_stage]

        return next_stage


class Stage(object):

    def __init__(self, name):
        self._name = name
        self._next_stage = name
        self._started = False
        self._actors = {}
        self._layers = {}

    def get_name(self):
        return self._name

    def add_actor(self, actor: Actor, layer=0):
        self._actors[actor.get_name()] = actor
        if not layer in self._layers:
            self._layers[layer] = []
        self._layers[layer].append(actor)

    def find_actor(self, actor_name: str) -> Actor:
        try:
            return self._actors[actor_name]
        except KeyError:
            return None

    def remove_actor(self, actor_name: str):
        del self._actors[actor_name]

    def start(self):
        self._started = True
        self._next_stage = self._name

    def is_started(self):
        return self._started

    def step(self, viewport, delta_time):
        self._update(delta_time)
        viewport.clear()
        self._draw(viewport)
        return self._next_stage

    def _update(self, delta_time):
        for actor in self._actors.values():
            actor.update(delta_time)

    def _draw(self, viewport):
        for layer_key in sorted(self._layers):
            for actor in self._layers[layer_key]:
                viewport.render_sprite(actor, actor.get_position())

    def stop(self):
        self._started = False
