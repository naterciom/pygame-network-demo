import pickle
from threading import Thread

import pygame
from twisted.internet import reactor
from twisted.internet.protocol import Protocol, ClientFactory

from game.client.actors.players import Player, RemotePlayer
from game.client.actors.tiles import TileGrid
from game.client.stages import Stage


class GameStage(Stage):

    def __init__(self, host, port):
        Stage.__init__(self, 'GAME')

        self._host = host
        self._port = port
        self._others = []

        player = Player()
        self._player = player
        self.add_actor(player)

        self._client_factory = GameClientFactory(player)

        tile_map = TileGrid(50, 50)
        self.add_actor(tile_map, -1)

    def start(self):
        super(GameStage, self).start()
        reactor.connectTCP(self._host, self._port, self._client_factory)
        Thread(target=reactor.run, args=(False,)).start()

    def _update(self, delta_time):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._next_stage = None

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self._next_stage = 'MAIN_MENU'

        self._update_remote_players()

        Stage._update(self, delta_time)

    def _draw(self, viewport):
        viewport.focus(self._player.get_position(), 0.1)

        Stage._draw(self, viewport)

    def _update_remote_players(self):
        for other_name, other_info in self._client_factory.get_client().get_others().items():
            remote_player = self.find_actor(other_name)

            if not remote_player:
                remote_player = RemotePlayer(name=other_name)
                self.add_actor(remote_player)

            remote_player.set_position(other_info.position)


class GameClient(Protocol):

    def __init__(self, player):
        self._player = player
        self._other_players = {}

    def get_others(self):
        return self._other_players

    def connectionMade(self):
        print('Connected to server')
        self.transport.write(pickle.dumps(self._player.get_info()))

    def dataReceived(self, data):
        self.transport.write(pickle.dumps(self._player.get_info()))
        other_player = pickle.loads(data)
        self._other_players[other_player.name] = other_player

    def connectionLost(self, parameter_list):
        print('Disconnected from server')


class GameClientFactory(ClientFactory):

    def __init__(self, player):
        self._client = GameClient(player)

    def get_client(self):
        return self._client

    def buildProtocol(self, address):
        return self._client

    def clientConnectionLost(self, connector, reason):
        print('Connection lost. Reason:', reason)

    def clientConnectionFailed(self, connector, reason):
        print('Connection failed. Reason:', reason)
