import pygame

from game.client.stages import Stage


class MainMenuStage(Stage):

    def __init__(self):
        Stage.__init__(self, 'MAIN_MENU')

    def _update(self, delta_time):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._next_stage = None

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self._next_stage = None
                if event.key == pygame.K_SPACE:
                    self._next_stage = 'GAME'

    def _draw(self, viewport):
        viewport.render_text('Press "SPACE" to start', (100, 50))
        viewport.render_text('Press "ESC" to quit', (100, 100))
