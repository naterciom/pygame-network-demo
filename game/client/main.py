import pygame
from twisted.internet import reactor

from game.client.common import Viewport
from game.client.stages import StageOrchestrator
from game.client.stages.game import GameStage
from game.client.stages.main_menu import MainMenuStage


def main(args=None, config=None):
    print('Starting client...')

    host = config['host']
    port = config['port']

    pygame.init()
    viewport = Viewport(pygame.display.set_mode(
        (800, 600), pygame.HWSURFACE | pygame.DOUBLEBUF),
        pygame.Rect((-800, -800), (1600, 1600)))
    clock = pygame.time.Clock()

    orchestrator = StageOrchestrator(viewport)
    orchestrator.add_stage(MainMenuStage())
    orchestrator.add_stage(GameStage(host, port))

    print('Client started!')
    while orchestrator.next_step(clock.tick(config['game']['framerate'])):
        viewport.render_text(str(int(clock.get_fps())), (0, 0))
        viewport.update()

    reactor.callFromThread(reactor.stop)
    pygame.quit()


if __name__ == '__main__':
    main()
