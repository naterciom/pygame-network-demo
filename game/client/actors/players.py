import pygame
from pygame.math import Vector2

from game.client.actors import Actor
from game.client.common import Color


class Player(Actor):

    def __init__(self, **kwargs):
        self._speed = Vector2(0, 0)
        surface = pygame.Surface((16, 16), pygame.HWSURFACE | pygame.SRCALPHA).convert_alpha()
        surface.fill(Color.TRANSPARENT)
        pygame.draw.circle(surface, Color.BRIGHT_GREEN, (8, 8), 8)
        Actor.__init__(self, surface, **kwargs)

    def setSpeed(self, x, y):
        self._speed.x = x
        self._speed.y = y

    def update(self, delta_time):
        self._controls()
        if self._speed.length() >= .001:
            time_coeff = delta_time / 1000
            movement = self._speed.length() * time_coeff
            self._speed.scale_to_length(movement)
            self._position += self._speed

    def _controls(self):
        pressed_keys = pygame.key.get_pressed()

        if pressed_keys[pygame.K_UP]:
            self._speed.y = -100
        elif pressed_keys[pygame.K_DOWN]:
            self._speed.y = 100
        else:
            self._speed.y = 0

        if pressed_keys[pygame.K_LEFT]:
            self._speed.x = -100
        elif pressed_keys[pygame.K_RIGHT]:
            self._speed.x = 100
        else:
            self._speed.x = 0


class RemotePlayer(Actor):

    def __init__(self, **kwargs):
        self._speed = Vector2(0, 0)
        surface = pygame.Surface((16, 16), pygame.HWSURFACE | pygame.SRCALPHA).convert_alpha()
        surface.fill(Color.TRANSPARENT)
        pygame.draw.circle(surface, Color.BRIGHT_RED, (8, 8), 8)
        Actor.__init__(self, surface, **kwargs)

    def update(self, delta_time: int):
        pass