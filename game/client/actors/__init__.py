import uuid
from abc import ABCMeta

from pygame import Surface
from pygame.math import Vector2


class Sprite(object):

    def __init__(self, surface: Surface):
        self._surface = surface
        self._pivot = Vector2(int(surface.get_width() / 2), int(surface.get_height() / 2))

    def draw(self, surface: Surface, position: (int, int)):
        offset_position = Vector2(position) - self._pivot
        surface.blit(self._surface, (int(offset_position.x), int(offset_position.y)))


class Behaviour(object):

    def update(self, delta_time: int):
        raise NotImplementedError


class ActorInfo(object):

    def __init__(self, name, position):
        self.name = name
        self.position = position


class Actor(Sprite, Behaviour, metaclass=ABCMeta):

    def __init__(self, surface: Surface, name=None):
        self._name = name if name else str(uuid.uuid4())
        self._position = Vector2(0, 0)

        Sprite.__init__(self, surface)
        Behaviour.__init__(self)

    def get_name(self):
        return self._name

    def get_position(self):
        return int(self._position.x), int(self._position.y)

    def set_position(self, position):
        self._position = Vector2(position)

    def get_info(self) -> ActorInfo:
        return ActorInfo(self._name, self.get_position())
