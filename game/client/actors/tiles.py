"""
Everything tiles related
"""
import pygame
from pygame import Surface
from pygame.math import Vector2

from game.client.actors import Actor
from game.client.common import Color, TerrainGenerator


class Tile(Actor):

    def __init__(self, elevation, surface, **kwargs):
        Actor.__init__(self, surface)
        self._elevation = elevation
        self._dirty = True

    def set_dirty(self, dirty=True):
        self._dirty = dirty

    def is_dirty(self):
        return self._dirty

    def update(self, delt_time):
        pass


class TileFactory(object):

    def __init__(self, size=32, border=1):
        self._size = size
        self._border = border

        self._tile_surfaces = {}

        self._tile_surfaces[0.5] = self._tile(Color.DARK_GREY)
        self._tile_surfaces[1.0] = self._tile(Color.GREY)

    def new_tile(self, position, elevation):
        x = 1.0
        for e in self._tile_surfaces.keys():
            if (e > elevation and e < x):
                x = e
        new = Tile(elevation, self._tile_surfaces[x])
        new.set_position(position)
        return new

    def _tile(self, color):
        total_size = self._size + self._border
        surface = Surface((total_size, total_size), pygame.HWSURFACE).convert()
        pygame.draw.rect(surface, Color.BLACK, pygame.Rect((0, 0), (total_size, total_size)), self._border)
        inner_tile = Vector2((0, 0)) + Vector2(self._border, self._border)
        pygame.draw.rect(surface, color, pygame.Rect((inner_tile.x, inner_tile.y), (self._size, self._size)))
        return surface


class TileGrid(Actor):

    def __init__(self, width, height, tile_size=32, **kwargs):
        self._width = width
        self._height = height
        self._pixel_width = width * tile_size
        self._pixel_height = height * tile_size
        self._tile_size = tile_size
        self._half_tile_size = tile_size / 2
        self._tiles = {}

        generator = TerrainGenerator()
        factory = TileFactory(tile_size, 1)
        for x in range(width):
            for y in range(height):
                elevation = generator.elevation_at((x, y))
                self._tiles[x, y] = factory.new_tile((x * tile_size, y * tile_size), elevation)

        surface = Surface((self._pixel_width, self._pixel_height), pygame.HWSURFACE).convert()
        surface.fill(Color.BLACK)
        for tile in self._tiles.values():
            tile.draw(surface, self._offset_position(tile.get_position()))

        Actor.__init__(self, surface, **kwargs)

    def tile_at(self, position):
        relative_position = Vector2(self.get_position()) + Vector2(position) - Vector2(self._pixel_width / 2,
                                                                                       self._pixel_height / 2)
        grid_position = relative_position / self._tile_size + Vector2(self._width / 2)

        if 0 <= grid_position.x <= self._width and 0 <= grid_position.y <= self._height:
            return self._tiles[round(grid_position.x), round(grid_position.y)]
        else:
            return None

    def update(self, delta_time):
        pass

    def _offset_position(self, position):
        (x, y) = position
        x += self._half_tile_size
        y += self._half_tile_size
        return (x, y)


class TileMap(Actor):
    TILE_SIZE = 32

    def __init__(self, viewer, view_distance, chunk_size):
        Actor.__init__(self)

        self._viewer = viewer
        self._view_distance = view_distance
        self._chunk_size = chunk_size
        self._chunk_pixel_size = chunk_size * TileMap.TILE_SIZE
        self._surface = Surface((self._chunk_pixel_size * (self._view_distance * 2 + 1),
                                 self._chunk_pixel_size * (self._view_distance * 2 + 1)), pygame.HWSURFACE)

        self._chunks = {}
        self._visible_chunks = {}
        self._last_chunk = None

    def update(self, delta_time):
        current_chunk = self._get_current_chunk()
        if self._last_chunk != current_chunk:
            self._update_chunks(current_chunk)

    def _update_chunks(self, current_chunk):
        old_visible_chunks = list(self._visible_chunks.keys())

        for chunk in rectIterator(current_chunk[0] - self._view_distance, current_chunk[1] - self._view_distance,
                                  2 * self._view_distance + 1):
            if chunk in self._visible_chunks.keys():
                old_visible_chunks.remove(chunk)
            else:
                (x, y) = chunk
                self._visible_chunks[chunk] = self._new_chunk(x, y)

        for chunk in old_visible_chunks:
            del self._visible_chunks[chunk]

        (chunk_x, chunk_y) = current_chunk
        self.set_position(self._calculate_position(current_chunk))

        self._last_chunk = current_chunk

    def _get_current_chunk(self):
        chunk = Vector2(self._viewer.get_position())
        chunk /= self._chunk_pixel_size
        return (round(chunk.x), round(chunk.y))

    def _calculate_position(self, current_chunk):
        x = current_chunk[0] * self._chunk_pixel_size - self._surface.get_width() / 2
        y = current_chunk[1] * self._chunk_pixel_size - self._surface.get_height() / 2
        return (x, y)

    def _new_chunk(self, x, y):
        chunk = TileGrid(self._chunk_size, self._chunk_size)
        chunk.set_position((x * 32 * self._chunk_size, y * 32 * self._chunk_size))
        return chunk

    def draw(self):
        for chunk in self._visible_chunks.values():
            self._surface.blit(chunk.draw(), chunk.get_position())

        return self._surface


def rectIterator(x, y, size):
    for xx in range(size + 1):
        for yy in range(size + 1):
            yield (x + xx, y + yy)
