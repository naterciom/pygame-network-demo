#!/usr/bin/env python

import argparse
import yaml


def main(args):
    with open(args.config, 'r') as yml_file:
        cfg = yaml.load(yml_file)

    if args.server:
        import game.server.main as app
    else:
        import game.client.main as app

    if args.profiler:
        import cProfile
        import pstats
        import datetime
        profile_file_name = 'profile-{}'.format(datetime.datetime.now())
        cProfile.runctx('app.main(args, cfg)', {}, {'app': app, 'args': args, 'cfg': cfg}, profile_file_name)
        pstats.Stats(profile_file_name).strip_dirs().sort_stats('cumtime').reverse_order().print_stats()
    else:
        app.main(args, cfg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Launch game')

    parser.add_argument('--server',
                        action='store_true',
                        help='launch game server')

    parser.add_argument('--config',
                        default='config.yml',
                        help='specify configuration yaml file')

    parser.add_argument('--profiler',
                        action='store_true',
                        help='launch with profiler')

    main(parser.parse_args())
